#!/bin/sh

#set -x

## internal variables

# branding
dbname='db.d'
cfg_obj='cfg.env'

# define undefined if necessary (potential set -u)
SDBU_DEBUG="${SDBU_DEBUG:-}"
INVALID_CHARS="-.,'[]/?!:\`()*\";{}+=<>~$|#@&–—"

## internal functions without prefix sdbu

sdbu__die() {
  echo "$@" >&2
  false
}

# src https://unix.stackexchange.com/questions/23111/what-is-the-eval-command-in-bash/250345#250345
dbg_var() {
  local var="${1}"
  eval echo "${var}: \$${var}"
}

env_before() {
  export -p > /tmp/env1
}
env_after() {
  printf "\nnew variables in this shell:\n\n"
  export -p > /tmp/env2
  grep -vxFf /tmp/env1 /tmp/env2
}

sdbu__obj_path_validation() {
  local obj_path="${1}"

  if [ -z "${obj_path}" ]; then
    sdbu__die "ERROR: missing obj path argument" || return 1
  fi

  # a directory should end with a slash, that facilitates its detection
  if [ ! "$(echo "${obj_path}" | grep -o '.$')" = '/' ]; then
    obj_path="${obj_path}/"
  fi

  if [ ! -d "${obj_path}" ]; then
    sdbu__die "ERROR: obj path ${obj_path} is not a directory" || return 1
  elif ! echo "${obj_path}" \
    | grep -E "/?${dbname}/" \
    | grep -F "${dbname}/" >/dev/null 2>&1; then
    sdbu__die "ERROR: db.d not found in obj path ${obj_path}" || return 1
  fi
}

## public functions to use

# read other object files in current directory
sdbu__read_other_objs() {
  obj_path="${1}"
  sdbu__obj_path_validation "${obj_path}" || return 1
  local cfg_objs="$(find "${obj_path}" -maxdepth 1 -type f | grep -v "${cfg_obj}"$)"
  if [ -n "${cfg_objs}" ]; then
    local my_obj=''
    while IFS= read -r my_obj <&9; do
      mykey_raw="$(basename "${my_obj}")"
      mykey="$(echo "${mykey_raw}" | tr -d -- "$INVALID_CHARS")"
      relpath_my_obj="$(echo "${my_obj}" | grep -o "${dbname}/.*")"
      # as read can only read one line, that line could be the file itself
      set -a
      read "${mykey}" <<LINE
${relpath_my_obj}
LINE
      set +a
      [ -n "${SDBU_DEBUG}" ] && printf "\n    ___read_other_objs() iteration__\n\n${mykey}: ${relpath_my_obj}\n\n"
    done 9<<END
${cfg_objs}
END
  fi
  true
}

# it scans obj until arrives to the root directory db.d
sdbu__goto_root() {
  # TODO improve validation
  local obj_path="${1}"
  scanner="${obj_path}"
  dir_chain=''

  sdbu__obj_path_validation "${obj_path}" || return 1

  traverse_limit_param="${traverse_limit_param:-50}"
  traverse_limit="${traverse_limit_param}"

  while true; do

    curr_dir="$(basename "${scanner}")"

    dir_chain="${curr_dir} ${dir_chain}"

    [ -n "${SDBU_DEBUG}" ] && printf "\n___init_goto_root() iteration__\n\nscanner: ${scanner}\ncurr_dir: ${curr_dir}\ndir_chain: ${dir_chain}\n\n"

    scanner="$(dirname "${scanner}")"
    if [ "${curr_dir}" = "${dbname}" ]; then
      fullpath="${scanner}"
      break
    elif [ "${traverse_limit}" -eq 0 ]; then
      text_error="ERROR: reached traverse_limit var, adjusted to ${traverse_limit_param} hops, cannot reach db.d root dir"
      sdbu__die  "${text_error}" || return 1

    fi
    traverse_limit="$(($traverse_limit-1))"
  done
}

sdbu__returnto_obj() {
  # TODO better validation of inputs
  dir_chain="${1}"
  fullpath="${2}"
  [ -n "${SDBU_DEBUG}" ] && env_before
  relpath_rebuild=''
  for curr_dir in ${dir_chain}; do
    if [ -z "${relpath_rebuild}" ]; then
      relpath_rebuild="${curr_dir}"
    else
      relpath_rebuild="${relpath_rebuild}/${curr_dir}"
    fi
    fullpath_rebuild="${fullpath}/${relpath_rebuild}"
    cfg_path="${fullpath_rebuild}/${cfg_obj}"
    [ -n "${SDBU_DEBUG}" ] && printf "\n___returnto_obj() iteration__\n\ncfg_path: ${cfg_path}\n\n"

    set -a
    if [ -f "${cfg_path}" ]; then
      . "${cfg_path}"
    fi
    set +a

    sdbu__read_other_objs "${fullpath_rebuild}"

  done
  [ -n "${SDBU_DEBUG}" ] && env_after
  true
}

sdbu__read_obj() {
  local obj_path="${1}"

  sdbu__obj_path_validation "${obj_path}" || return 1

  sdbu__goto_root "${obj_path}"

  # return to object using the directory chain
  sdbu__returnto_obj "${dir_chain}" "${fullpath}"
}

# enable posix mode in case of bash
if [ "$0" = "bash" ]; then
  set -o posix
fi
