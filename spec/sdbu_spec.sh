Describe 'sdbu.sh'

  Include ./sdbu.sh

  Describe 'sdbu__obj_path_validation'
    It 'fails because it misses argument'
      When call sdbu__obj_path_validation
      The status should be failure
      The stderr should equal "ERROR: missing obj path argument"
    End
    It 'fails because arg is not a directory'
      obj_path='/fake/path/'
      When call sdbu__obj_path_validation "${obj_path}"
      The status should be failure
      The stderr should equal "ERROR: obj path ${obj_path} is not a directory"
    End
    It 'fails because it does not contain db.d directory'
      obj_path='/tmp'
      When call sdbu__obj_path_validation "${obj_path}"
      The status should be failure
      The stderr should equal "ERROR: db.d not found in obj path ${obj_path}/"
    End
    It 'suceeds because db.d directory is included as relative path'
      # forcing a situation different than the typical spec_files/db.d
      cd spec_files
      obj_path='db.d'
      When call sdbu__obj_path_validation "${obj_path}"
      The status should be success
    End
  End

  Describe 'sdbu__read_obj()'
    It 'succeeds because argument is appropriate (db.d)'
      obj_path='spec_files/db.d'
      When call sdbu__obj_path_validation "${obj_path}"
      The status should be success
    End
    It 'succeeds because argument is appropriate (db.d/)'
      obj_path='spec_files/db.d/'
      When call sdbu__obj_path_validation "${obj_path}"
      The status should be success
    End
  End

  Describe 'sdbu__goto_root()'
  End

  Describe 'sdbu__read_other_objs()'
    It 'succeeds because argument is appropriate (db.d)'
      obj_path='spec_files/db.d'
      When call sdbu__read_other_objs "${obj_path}"
      The status should be success
    End
  End

  Describe 'sdbu__returnto_obj()'
  End
End
